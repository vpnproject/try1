import socket
# import select
import dpkt
from dpkt.compat import compat_ord
from termcolor import colored
import argparse
import re


def get_mac_address(mac):
    """
    input- mac address from packet, in str
    output- mac address ex. 'aa:bb:cc:dd:ee:ff'
    """
    return ':'.join('%02x' % compat_ord(b) for b in mac)


def send_all(data, address):
    """
    input- packet to send, and sender address
    output- prints a message to which client a packet was sended
    sends the packet to all clients but the sender himself
    """
    global SERVER_SOCKET
    net = get_network(address)
    for client in NETWORKS_IP_PORT[net]:
        if address != client:
            SERVER_SOCKET.sendto(data, client)
            print "Sending message to", client


def setup_socket():
    """
    input- no input
    output- no output
    sets uo the socket to UDP socket, and binds it
    """
    global BIND_IP
    global PORT
    global SERVER_SOCKET
    SERVER_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    SERVER_SOCKET.bind((BIND_IP, PORT))
    return SERVER_SOCKET


def exit_client():
    """
    input- address of exiting client through variable 'ADDRESS'
    output- prints message which client exited
    erases client from it's network list, and it's 'mac:address' dictionary
    erases the clients packet (which is 'Error')
    """
    global ADDRESS
    global NETWORKS_IP_PORT
    net = get_network(ADDRESS)
    if net == 0:  # client does not exist
        return

    print colored("Client {0} exited from network {1}".format(ADDRESS, net), 'cyan')

    for i in range(0, len(NETWORKS_IP_PORT[net])):
        if NETWORKS_IP_PORT[net][i] == ADDRESS:
            NETWORKS_IP_PORT[net].pop(i)
            break

    for key in NETWORKS_MAC[net].keys():
        if NETWORKS_MAC[net][key] == ADDRESS:
            del NETWORKS_MAC[net][key]
            break


def is_new():
    global SERVER_SOCKET
    global ADDRESS
    SERVER_SOCKET.sendto('???',ADDRESS)
    dat, adr = SERVER_SOCKET.recvfrom(65535)
    dat = dat[1:]
    if dat == 'Y':
        return True
    if dat[:4] != 'id: ':
        back_up = ADDRESS  # got a packet from different level
        ADDRESS = adr
        handle_message(dat)
        ADDRESS = back_up
        return is_new()
    old = VIRTUAL_IP[dat[4:]]
    net = get_network(old)
    print 'client {0} in network {1} changed port'.format(adr, net)
    for i in range(0, len(NETWORKS_IP_PORT[net])):
        if NETWORKS_IP_PORT[net][i] == old:
            NETWORKS_IP_PORT[net][i] = adr
            break

    for key in NETWORKS_MAC[net].keys():
        if NETWORKS_MAC[net][key] == old:
            NETWORKS_MAC[net][key] = adr
            break

    VIRTUAL_IP[dat[4:]] = adr

    return False


def add_new_client(data):
    """
    input- clients address through 'ADDRESS'
    output- prints which client entered (ip and port)
    erases clients packet
    """
    global ADDRESS
    global NETWORKS_IP_PORT
    global NETWORKS_MAC
    global VIRTUAL_IP
    global SERVER_SOCKET
    global ID
    global NETWORKS_PASS
    net = get_network(ADDRESS)

    if net != 0:
        return  # client exists
    if not is_new():
        return

    a = re.match('(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\s(\w*)', data)
    d = a.group(1).split('.')[0:3]
    d.append('0')
    d = '.'.join(d)

    if d in NETWORKS_IP_PORT.keys():  # if network exists
        if a.group(2) == NETWORKS_PASS[d]:  # if password correct
            NETWORKS_IP_PORT[d].append(ADDRESS)  # add new client
        else:
            SERVER_SOCKET.sendto('ERR', ADDRESS)
            return

    else:  # create new network
        print "added new network at {0} password {1}!".format(d, a.group(2))
        NETWORKS_IP_PORT[d] = [ADDRESS]
        NETWORKS_MAC[d] = {}
        NETWORKS_PASS[d] = a.group(2)
    SERVER_SOCKET.sendto(str(ID), ADDRESS)
    VIRTUAL_IP[str(ID)] = ADDRESS
    print colored('Client {0} joined to network {1}'.format(ADDRESS, get_network(ADDRESS)), 'blue')
    ID += 1


def exit_or_enter_client(data):
    """
    input- clients message trough 'DATA'
    output- no output
    checks if the clients is new, or if it is exiting
    """
    if data == "Exit":
        exit_client()
    else:
        add_new_client(data)


def add_mac_to_dic(mac_add, data):
    """
    input- src mac address in 'DATA'
    output- no output
    adds clients nac address to dictionary
    """
    global ADDRESS
    global NETWORKS_MAC
    net = get_network(ADDRESS)
    if net in NETWORKS_MAC.keys():  # network exists?
        if mac_add in NETWORKS_MAC[net].keys():  # client mac exists already
            return
        NETWORKS_MAC[net][mac_add] = ADDRESS  # enter new client's mac
        return

    NETWORKS_MAC[net] = {mac_add: ADDRESS}


def send_messages_to_clients(data, dst):
    """
    input- source ip 'dst'
    output- no output
    if dst ip is a client- sends it to him
    else- sends to all clients
    """
    global ADDRESS
    global SERVER_SOCKET
    net = get_network(ADDRESS)
    net = NETWORKS_MAC[net]
    if dst in net.keys():
        SERVER_SOCKET.sendto(data, (net[dst]))
    else:
        send_all(data, ADDRESS)


def get_header(pkt):
    return pkt[0]


def get_network(address):
    """
    input- address ip/port
    output- returns the address' network
    if address is not in any network- return 0
    """
    global NETWORKS_IP_PORT
    for net in NETWORKS_IP_PORT.keys():
        for i in range(len(NETWORKS_IP_PORT[net])):
            if NETWORKS_IP_PORT[net][i] == address:
                return net
    return 0


def handle_message(data):
    """
    input- src ip through 'ADDRESS' and data through 'DATA'
    output- prints the senders' ip, and the src and dst mac address
    sends the packet to its' destiny
    """
    global ADDRESS
    if get_header(data) == SCRIPT_HEADER:
        exit_or_enter_client(data[1:])
    if get_header(data) == DEV_HEADER:
        send(data[1:])


def send(data):
    global ADDRESS
    if data != '':
        try:
            print colored('Got message from computer at {0}:'.format(ADDRESS), 'green')
            add = dpkt.ethernet.Ethernet(data)
            add_mac_to_dic(add.src, data)
            print colored(("    Src mac {0} Dst mac {1}".format(get_mac_address(add.src), get_mac_address(add.dst))),
                          'red')
            dst = get_mac_address(add.dst)
            send_messages_to_clients(data, dst)
        except dpkt.Error:
            print ('Error in message')


def parse():
    """
    input- no input
    output- no output
    gets variables from the cmd
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-port', help='port to connect to clients', type=int, default=5678)
    parser.add_argument('-bind_ip', help='ip to bind socket', type=str, default='0.0.0.0')
    parser.add_argument('-max_size', help='max size buffer to read from socket', type=int, default=65535)
    arguments = parser.parse_args()
    return arguments


def main():
    parse()
    global MAX_SIZE_READ
    global ADDRESS
    global SERVER_SOCKET
    global NETWORKS_IP_PORT
    SERVER_SOCKET = setup_socket()
    while True:
        DATA, ADDRESS = SERVER_SOCKET.recvfrom(MAX_SIZE_READ)
        handle_message(DATA)


args = parse()
PORT = args.port
BIND_IP = args.bind_ip
MAX_SIZE_READ = args.max_size
DATA = ''
ADDRESS = ''
VIRTUAL_IP = {}
NETWORKS_IP_PORT = {}  # all networks: [network1]= [all network1 clients]
NETWORKS_MAC = {}  # all networks: [network1] = {mac: address}
NETWORKS_PASS = {}
SERVER_SOCKET = socket.socket()
SCRIPT_HEADER = '1'
DEV_HEADER = '2'
ID = 0

if __name__ == '__main__':
    main()


class Network(object):
    def __int__(self):
        self.addresses = []
        self.macs = {}

    def add_address(self, name):
        self.addresses.append(name)

    def add_mac(self, mac, address):
        self.macs[mac] = address
