import sys
import socket
import select
import pytun
from termcolor import colored
import threading
import os
from time import sleep
import dpkt
import argparse
import hashlib


def http():
    """run simple HTTP server"""
    print colored("\ropening HTTP server at port 8080", "green")
    os.popen('python -m SimpleHTTPServer 8080')  # run this line in command line, starts SimpleHTTPServer onport 8000



def get_packet_type(packet, my_ip):
    """return a message according to the type of packet and where it is going"""
    pkt = dpkt.ethernet.Ethernet(packet)
    if isinstance(pkt.data, dpkt.ip.IP):
        ip_sec = pkt.data
        if isinstance(ip_sec.data, dpkt.icmp.ICMP):
	    icmp_pkt = ip_sec.data
            if socket.inet_ntoa(ip_sec.src) == my_ip and icmp_pkt.type == 8:
                return "pinging {0}".format(socket.inet_ntoa(ip_sec.dst))
            if socket.inet_ntoa(ip_sec.src) != my_ip and icmp_pkt.type == 8:
                return "pinged by {0}".format(socket.inet_ntoa(ip_sec.src))
            if socket.inet_ntoa(ip_sec.dst) == my_ip and icmp_pkt.type == 0:
                return "ping answered by {0}".format(socket.inet_ntoa(ip_sec.src))
            else:
                return "answering ping from {0}".format(socket.inet_ntoa(ip_sec.dst))
	else:
	    return "don't recongnize packet, but it goes like this: {0}-->{1}".format(socket.inet_ntoa(ip_sec.src), socket.inet_ntoa(ip_sec.dst))
    elif isinstance(pkt.data, dpkt.arp.ARP):
	arp_sec = pkt.data
        if socket.inet_ntoa(arp_sec.spa) == my_ip and arp_sec.op == 1:
            return "searching for {0}".format(socket.inet_ntoa(arp_sec.tpa))
	elif socket.inet_ntoa(arp_sec.spa) != my_ip and arp_sec.op == 1:
            return "you're being arp'ed by {0}".format(socket.inet_ntoa(arp_sec.spa))
	elif socket.inet_ntoa(arp_sec.spa) != my_ip and arp_sec.op == 2:
            return "found {0}!".format(socket.inet_ntoa(arp_sec.spa))
        else:
            return "replying to {0}!".format(socket.inet_ntoa(arp_sec.tpa))
        # elif isinstance(ip_sec.data, dpkt.tcp.TCP):
        #     tcp = ip_sec.data
        #     try:
        #         http1 = dpkt.http.request(tcp)
        #         print http1
        #     except (dpkt.dpkt.NeedData, dpkt.dpkt.UnpackError):
        #         pass
    else:
	return "not supported packet type"


class TunnelServer(object):
    # this is original line: def __init__(self, taddr, tmask, tmtu, laddr, lport, raddr, rport):
    def __init__(self, taddr, tmask, tmtu, raddr, rport, pswrd, mname):
        self._tap = pytun.TunTapDevice(name=mname,
                                       flags=pytun.IFF_TAP | pytun.IFF_NO_PI)  # create a tap device with the name Aharon
        self._tap.addr = taddr  # set address of tap device
        self._tap.netmask = tmask  # set netmask of tap device
	self._word = pswrd
        a = taddr.split(".")
        self._tap.dstaddr = ".".join(a)
        self._tap.mtu = tmtu  # set mut of tap device
        self._tap.up()  # start tap device
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # create a UDP socket
        self._raddr = raddr  # set server IP address to send to
        self._rport = rport  # set Server port to send to
        print colored("this is your IP address and port: ({0}, {1})".format(self._tap.addr, self._rport), "green")
        #threading.Thread(target=http).start()  # run SimpleHTTPServer
        raw_input(colored("hit enter to connect", "yellow"))
	a[3] = '0'
	while not self.connect('.'.join(a)):
	    pass
        print colored("to end use ctrl c", "yellow")
        sleep(2)  # here so you see the second print


    def connect(self, name):
	self._word = raw_input("enter password")
	self._sock.sendto('1'+name+' '+hashlib.sha224(self._word).hexdigest(), (self._raddr, self._rport))
	to_tap, addr = self._sock.recvfrom(65535) 
 # move the recived message to a variable so we can put it in the tap device
	self._sock.sendto('1'+"Y", (self._raddr, self._rport))
	i = self._sock.recvfrom(65535)[0]
	if i == 'ERR':
	    return False
	self._id = i
	return True


    def run(self):

        x = []
        to_sock = ''
        to_tap = ''
        while True:
            try:
                r = [self._tap, self._sock]
                w = [self._tap, self._sock]
                r, w, x = select.select(r, w, x)  # get the items that can be read from, and the ones that can write

                if self._tap in r:  # we have a message in the tap device, so we move it to socket to send
                    to_sock = '2'+self._tap.read(self._tap.mtu)  # put the message in a variable to send through socket
                    # print colored((to_sock, "tap message"), 'blue')
                    # print colored((dpkt.ethernet.Ethernet(to_sock), "tap"), "blue")
                    print colored(get_packet_type(to_sock, self._tap.addr), "blue")
                if self._sock in r:  # we recived a message from the socket, so we move it to the tap device
                    to_tap, addr = self._sock.recvfrom(
                        65535)  # move the recived message to a variable so we can put it in the tap device
                    if to_tap == "???":
                        print colored("you swapped ports. don't worry, we took care of this for you :)", "red")
                        self._sock.sendto('1'+'id: '+self._id, (self._raddr, self._rport))
                        to_tap = ""
                    else:
		        print colored(get_packet_type(to_tap, self._tap.addr), "blue")
                    # print colored((to_tap, "sock message"), 'red')
                    # print dpkt.ethernet.Ethernet(to_tap)
                    if addr[0] != self._raddr or addr[
                        1] != self._rport:  # if the message came from our IP, then erase it
                        to_tap = ''

                if self._tap in w and to_tap:  # if the tap can recive a message and there is a message for it to get, give the tap the message
                    self._tap.write(to_tap)  # write the message to the tap
                    to_tap = ''  # erase the message so it does not get sent again

                if self._sock in w and to_sock:
                    self._sock.sendto(to_sock, (
                        self._raddr, self._rport))  # send the message to the server, so it can send it to where we want
                    to_sock = ''  # erase the message so it does not get sent again

            except(KeyboardInterrupt), e:  # if we hit ctrl c, we disconnect from the server
                self._sock.sendto('1'+"Exit", (
                    self._raddr, self._rport))  # sending "Exit" to the server disconnects us from the server
                return 0
                #           except (select.error, socket.error, pytun.Error), e:
                #               if e[0] == errno.EINTR:
                #                   continue
                #               print >> sys.stderr, str(e)
                #               break


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("server_IP", help="IP of the server")
    parser.add_argument("--password", help="password of network", default='')
    parser.add_argument("--name", help="name of virtual card", default='VPN')
    parser.add_argument("--IP", help="IP of your virtual card", default="10.0.0.55")
    parser.add_argument("--netmask", help="netmask of your IP", default="255.255.255.0")
    args = parser.parse_args()
    print colored("hi", 'blue')
    try:
        # server = TunnelServer("10.0.1.55", "255.255.255.0", 1200, "79.182.137.136", 5678)
        server = TunnelServer(args.IP, args.netmask, 1200, args.server_IP, 5678, args.password, args.name)
    except (pytun.Error,
            socket.error), e:  # if there is an error is starting the tap device or the socket, then exit and print the error
        print >> sys.stderr, str(e)
        return 1
    server.run()  # start the main part of the program, where all the sending of the messages happens
    return 0


if __name__ == '__main__':
    main()  # start the program
